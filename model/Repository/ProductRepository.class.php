<?php
/**
 * ProductRepostry fpr Product Model
 * @author     Rafal Wesolowski <wesolowski@nexus-netsoft.com>
 * @version    1.0
 */
class ProductRepository
{
    /**
     * Get all product
     *
     * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
     * @return object
     */
    public function getAllProduct()
    {
        return Model::factory('product')->find_many();
    }

    public function getProduct($id)
    {
        return Model::factory('product')->find_one($id);
    }

    public function getProduct_byNameAndCost($name, $cost)
    {
        return Model::factory('product')
                ->where('name',$name)
                ->where('cost',$cost)
                ->find_one();
    }

    public function getProduct_byName($name)
    {
        return Model::factory('product')
                ->where('name',$name)
                ->find_one();
    }

    public function createProduct($name, $description, $cost)
    {
        $new = true;
        foreach ($this->getAllProduct() as $oProduct) {
            if($oProduct->name == $name){
                $new = false;
            }
        }

        if($new){
            $product = Model::factory('product')->create();
            $product->name = $name;
            $product->description = $description;
            $product->cost = $cost;
            $product->save();
            $message = "Produkt wurde erfolgreich erstellt.";
        }else{
            $message = "Das Produkt existiert bereits.";
        }
        return $message;
    }

}