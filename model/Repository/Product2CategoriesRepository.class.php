<?php
/**
 * Product2CategoriesRepository for Product2Categories Model
 * @author     Rafal Wesolowski <wesolowski@nexus-netsoft.com>
 * @version    1.0
 */
class Product2CategoriesRepository
{
    public function getAllProduct2Category()
    {
        return Model::factory('Product2Categories')->find_many();
    }

    public function getOneProduct2Category($proID, $catID)
    {
        return Model::factory('Product2Categories')
                    ->where('product_ID', $proID)
                    ->where('category_ID', $catID)
                    ->find_one();
    }

    public function getManyProduct4Category($catID)
    {
         return Model::factory('Product2Categories')
                    ->where('category_ID', $catID)
                    ->find_many();
    }

    public function getManyCategory4Product($proID)
    {
         return Model::factory('Product2Categories')
                    ->where('product_ID', $proID)
                    ->find_many();
    }

    public function createProduct2Category($proID, $catID)
    {
        $new = true;
        foreach ($this->getAllProduct2Category() as $oPro2Cat) {
            if($oPro2Cat->proID == $proID && $oPro2Cat->catID == $catID){
                $new = false;
            }
        }

        if($new){
            $P2C = Model::factory('Product2Categories')->create();
            $P2C->product_ID = $proID;
            $P2C->category_ID = $catID;
            $P2C->save();
            $message = "Produkt wurde erfolgreich der Kategorie hinzugefügt.";
        }else{
            $message = "Produkt wurde bereits der Kategorie hinzugefügt.";
        }
        return $message;
    }

//    public function deleteProduct2Category($proID, $catID)
//    {
//        $P2C = $this->getProduct2Category($proID, $catID);
//
//        if( $P2C && $P2C->delete()){
//            $message = "Das Produkt wurde erfolgreich aus der Kategorie gelöscht.";
//        }else{
//            $message = "Das Produkt wurde nicht aus der Kategorie gelöscht.";
//        }
//        return $message;
//    }


}