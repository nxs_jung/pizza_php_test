<?php
/**
 * CategoryRepository for Category Model
 * @author     Rafal Wesolowski <wesolowski@nexus-netsoft.com>
 * @version    1.0
 */
class CategoryRepository
{
    /**
     * Get all categories
     *
     * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
     * @return object
     */
    public function getAllCategory()
    {
        return Model::factory('category')->find_many();
    }

    public function getCategory($id)
    {
        return Model::factory('category')->find_one($id);
    }

    public function getCategory_byName($name)
    {
        return Model::factory('category')->where('name', $name)->find_one();
    }

    public function createCategory($name, $pic)
    {
        $new = true;
        foreach ($this->getAllCategory() as $oCategory) {
            if($oCategory->name == $name){
                $new = false;
            }
        }

        if($new){
            $category = Model::factory('category')->create();
            $category->name = $name;
            $category->picture = $pic;
            $category->save();
            $message = "Kategorie wurde erfolgreich erstellt.";
        }else{
            $message = "Die Kategorie existiert bereits.";
        }
        return $message;
    }

    public function deleteCategory($id)
    {
        $category = Model::factory('category')->find_one($id);
        if( $category && $category->delete()){
            $message = "Kategorie wurde erfolgreich gelöscht.";
        }else{
            $message = "Die Kategorie wurde nicht gelöscht.";
        }
        return $message;
    }


}