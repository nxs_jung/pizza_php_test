<?php
/**
 * Category ORM Model
 * @author     Moritz Jung <jung@nexus-netsoft.com>
 * @version    1.0
 */
class Category extends Model {

    public static $_table = 'categories';

}