<?php

/**
 * (Controller-) Class for Admin-View
 * @author     Rafal Wesolowski <wesolowski@nexus-netsoft.com>
 * @version    1.0
 */
class Backend extends Controller  {

    /**
     * Renders the admin page
     *
     * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
     * @return void
     */
    public function showProduct()
    {
        $oProductInfo = $this->_getProductReposirty()->getAllProduct();

        $this->addTplParam( 'oProductInfo', $oProductInfo );
    }

    public function showCategories()
    {
        $oCategoryInfo = $this->_getCategoryRepository()->getAllCategory();

        $this->addTplParam( 'oCategoryInfo', $oCategoryInfo );
    }

    public function showBackend()
    {
        $this->showProduct();
        $this->showCategories();
        $this->addTplParam( 'url', 'menu-pizza.html' );
        $this->addTplParam( 'admin', 'true' );

        $this->render('admin');
    }

    public function addCategory()
    {
        if(isset($_POST['submit'])){
            $name = $_POST['name'];
            $picture = $_POST['picture'];
            $oNewInfo = $this->_getCategoryRepository()->createCategory($name, $picture);
            $this->addTplParam( 'oNewInfo', $oNewInfo );

            $this->showBackend();
        }else{
            $this->render('add_category');
        }
    }

    public function deleteCategory()
    {
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $oNewInfo = $this->_getCategoryRepository()->deleteCategory($id);
        }else{
            $oNewInfo = "Es wurde keine Kategorie ausgewählt.";
        }

        $this->addTplParam( 'oNewInfo', $oNewInfo );
        $this->showBackend();
    }

    public function editCategory()
    {
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $oCategoryInfo = $this->_getCategoryRepository()->getCategory($id);
        }else{
            $oCategoryInfo = "Es wurde keine Kategorie ausgewählt.";
        }

        $this->showProduct();

        $this->addTplParam( 'P2C', $this->_getProduct2CategoriesRepository() );
        $this->addTplParam( 'oCategoryInfo', $oCategoryInfo );
        $this->render('edit_category');
    }

    public function addProToCat()
    {
        if(isset($_GET['proID']) && isset($_GET['catID'])){
            $proID = $_GET['proID'];
            $catID = $_GET['catID'];

            $oCategoryAddInfo = $this->_getProduct2CategoriesRepository()->createProduct2Category($proID, $catID);

            $_GET['id'] = $catID;
            $sVerweis = 'editCategory';
        }else{
            $oCategoryAddInfo = "Es wurde keine Kategorie/kein Produkt ausgewählt.";
            $sVerweis = 'showBackend';
        }

        $this->addTplParam( 'oCategoryAddInfo', $oCategoryAddInfo );
        $this->$sVerweis();
    }

    public function export()
    {
        $this->render('export');
    }

    public function exportProductsXML()
    {
        $aProductInfo = $this->exportProdcutsArray();

        $doc = new DOMDocument('1.0');
        $doc->formatOutput = true;

        $products = $doc->createElement('products');
        $products = $doc->appendChild($products);

        foreach ($aProductInfo as $product_nr => $product) {
            $product_node = $doc->createElement('product');
            $product_node = $products->appendChild($product_node);

            foreach ($product as $key => $value) {
                $$key = $doc->createElement($key);
                $$key = $product_node->appendChild($$key);

                if(!is_array($value)){
                    $$value = $doc->createTextNode($value);
                    $$value = $$key->appendChild($$value);
                }else{
                    foreach ($value as $index => $value_cat) {
                        $category = $doc->createElement('category');
                        $category = $$key->appendChild($category);

                        $$value_cat = $doc->createTextNode($value_cat);
                        $$value_cat = $category->appendChild($$value_cat);
                    }
                }
            }
        }

        $doc->save("products.xml");
        $this->showBackend();
    }

    private function exportProdcutsArray()
    {
        $oProductInfos = $this->_getProductReposirty()->getAllProduct();

        $i = 0;

        foreach ($oProductInfos as $oProductInfo) {
            $aProductInfo[$i]['name'] = $oProductInfo->name;
            $aProductInfo[$i]['description'] = $oProductInfo->description;
            $aProductInfo[$i]['cost'] = $oProductInfo->cost;

            $aProductCategories = $this->_getProduct2CategoriesRepository()->getManyCategory4Product($oProductInfo->id);

            foreach ($aProductCategories as $oProductCategory) {
                if($this->_getCategoryRepository()->getCategory($oProductCategory->category_id)){
                    $aProductInfo[$i]['categories'][] = $this->_getCategoryRepository()->getCategory($oProductCategory->category_id)->name;
                }
            }

            $i++;
        }
        return $aProductInfo;
    }

    public function exportCategoriesXML()
    {
        $aCategoryInfo = $this->exportCategoriesArray();

        $doc = new DOMDocument('1.0');
        $doc->formatOutput = true;

        $categories = $doc->createElement('categories');
        $categories = $doc->appendChild($categories);

        foreach ($aCategoryInfo as $product_nr => $product) {
            $category_node = $doc->createElement('category');
            $category_node = $categories->appendChild($category_node);

            foreach ($product as $key => $value) {
                $$key = $doc->createElement($key);
                $$key = $category_node->appendChild($$key);

                if(!is_array($value)){
                    $$value = $doc->createTextNode($value);
                    $$value = $$key->appendChild($$value);
                }else{
                    foreach ($value as $index => $value_product) {
                        $category = $doc->createElement('product');
                        $category = $$key->appendChild($category);

                        $$value_product = $doc->createTextNode($value_product);
                        $$value_product = $category->appendChild($$value_product);
                    }
                }
            }
        }

        $doc->save("category.xml");
        $this->showBackend();
    }

    private function exportCategoriesArray()
    {
        $oCategoryInfos = $this->_getCategoryRepository()->getAllCategory();

        $i = 0;

        foreach ($oCategoryInfos as $oCategoryInfo) {
            $aCategoryInfo[$i]['name'] = $oCategoryInfo->name;

            $aCategoryProducts = $this->_getProduct2CategoriesRepository()->getManyProduct4Category($oCategoryInfo->id);

            foreach ($aCategoryProducts as $oCategoryProduct) {
                $aCategoryInfo[$i]['products'][] = $this->_getProductReposirty()->getProduct($oCategoryProduct->product_id)->name;
            }

            $i++;
        }
        return $aCategoryInfo;
    }

    public function import()
    {
        $this->render('import');
    }

    private function hasChild($p) {
        if ($p->hasChildNodes()) {
          foreach ($p->childNodes as $c) {
           if ($c->nodeType == XML_ELEMENT_NODE)
            return true;
          }
        }
        return false;
    }

    public function uploadProductXML()
    {
        if(isset($_POST['submit'])){
            $upload_dir = dirname(__DIR__) . DIRECTORY_SEPARATOR . "xml_upload" . DIRECTORY_SEPARATOR;
            $upload_file = $upload_dir . basename($_FILES['file_upload']['name']);
            if(move_uploaded_file($_FILES['file_upload']['tmp_name'], $upload_file)){
                $this->importProductXML($upload_file);
                $this->showBackend();
            }else{
                echo "Fehler beim Laden";
            }
        }else{
            $this->render('upload_pro_xml');
        }
    }

    public function importProductXML($xml_datei_name)
    {
        $xmlContent = file_get_contents($xml_datei_name);
        $dom = new DOMDocument;
        $dom->loadXML($xmlContent);
        $products = $dom->getElementsByTagName('product');

        foreach ($products as $key => $value)
        {
            foreach ($value->childNodes as $oChild)
            {
                if($this->hasChild($oChild))
                {
                    foreach ($oChild->childNodes as $oChildNodes) {
                        if($oChildNodes->nodeType == 1)
                        {
                            echo $oChild->nodeName . "<br>";
                            echo $oChildNodes->nodeName . ' -> ' .  $oChildNodes->nodeValue . "<br>";
                            $categories[] = $oChildNodes->nodeValue;
                        }
                    }
                }else{
                    if($oChild->nodeType == 1)
                    {
                        echo $oChild->nodeName . ' -> ' .  $oChild->nodeValue . "<br>";
                        $bez = $oChild->nodeName;
                        $$bez = $oChild->nodeValue;
                    }
                }
            }
            $this->addProductFromXML($name, $description, $cost, $categories);
        }
    }


    public function addProductFromXML($name, $description, $cost, $categories)
    {
        $sNewInfo = $this->_getProductReposirty()->createProduct($name, $description, $cost);
        $oNewProduct = $this->_getProductReposirty()->getProduct_byNameAndCost($name, $cost);
        foreach ($categories as $catName) {
            $sNewProToCat = $this->addCatToProFromXML($oNewProduct->id, $catName);
        }
    }

    public function addCatToProFromXML($proID, $catName)
    {
        $oCategory = $this->_getCategoryRepository()->getCategory_byName($catName);
        $catID = $oCategory->id;
        $sProductAddInfo = $this->_getProduct2CategoriesRepository()->createProduct2Category($proID, $catID);
        return $sProductAddInfo;
    }

    public function uploadCategoryXML()
    {
        if(isset($_POST['submit'])){
            $upload_dir = dirname(__DIR__) . DIRECTORY_SEPARATOR . "xml_upload" . DIRECTORY_SEPARATOR;
            $upload_file = $upload_dir . basename($_FILES['file_upload']['name']);
            if(move_uploaded_file($_FILES['file_upload']['tmp_name'], $upload_file)){
                $this->importCategoryXML($upload_file);
                $this->showBackend();
            }else{
                echo "Fehler beim Laden";
            }
        }else{
            $this->render('upload_cat_xml');
        }
    }

    public function importCategoryXML($xml_datei_name)
    {
        $xmlContent = file_get_contents($xml_datei_name);
        $dom = new DOMDocument;
        $dom->loadXML($xmlContent);
        $categories = $dom->getElementsByTagName('category');

        foreach ($categories as $key => $value)
        {
            foreach ($value->childNodes as $oChild)
            {
                if($this->hasChild($oChild))
                {
                    foreach ($oChild->childNodes as $oChildNodes) {
                        if($oChildNodes->nodeType == 1)
                        {
                            $products[] = $oChildNodes->nodeValue;
                        }
                    }
                }else{
                    if($oChild->nodeType == 1)
                    {
                        $bez = $oChild->nodeName;
                        $$bez = $oChild->nodeValue;
                    }
                }
            }
            $this->addCategoryFromXML($name, $picture, $products);
        }
    }


    public function addCategoryFromXML($name, $picture, $products)
    {
        $sNewInfo = $this->_getCategoryRepository()->createCategory($name, $picture);
        $oNewCategory = $this->_getCategoryRepository()->getCategory_byName($name);
        foreach ($products as $proName) {
            $sNewProToCat = $this->addProToCatFromXML($oNewCategory->id, $proName);
        }
    }

    public function addProToCatFromXML($catID, $proName)
    {
        $oProduct = $this->_getProductReposirty()->getProduct_byName($proName);
        $proID = $oProduct->id;
        $sCategoryAddInfo = $this->_getProduct2CategoriesRepository()->createProduct2Category($proID, $catID);
        return $sCategoryAddInfo;
    }
}