<?php
/**
 * (Controller-) Class for Public-View
 *
 * @author     Rafal Wesolowski <wesolowski@nexus-netsoft.com>
 * @version    1.0
 */
class Frontend extends Controller {

    /**
     * Renders the start page
     *
     * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
     * @return void
     */
    public function homeAction()
    {
        $this->addTplParam( 'url', 'menu-pizza.html' );
        $this->render('index');
    }

    public function showCategories()
    {
        $oCategoryInfo = $this->_getCategoryRepository()->getAllCategory();

        $this->addTplParam( 'oCategoryInfo', $oCategoryInfo );
    }

    /**
     * Renders the main page
     *
     * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
     * @return void
     */
    public function mainAction()
    {
        $sNameSpies = $_GET['spies'];
        if(isset($_GET['cat_ID'])){
            $iCatID = $_GET['cat_ID'];
        }else{
            $iCatID = $this->_getCategoryRepository()->getCategory_byName($sNameSpies)->id;
        }

        $this->showCategories();
        $aP2CInfo = $this->_getProduct2CategoriesRepository()->getManyProduct4Category($iCatID);

        foreach ($aP2CInfo as $oP2CInfo) {
            $oProductInfo[] = $this->_getProductReposirty()->getProduct($oP2CInfo->product_id);
        }

        $this->addTplParam( 'sNameSpies', $sNameSpies );
        $this->addTplParam( 'oProductInfo', $oProductInfo );

        $this->render('menu');
    }

}