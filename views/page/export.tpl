{include file='../layout/header.tpl'}
    <div id="main" class="content" role="main">
        <section class="main">
            <ul>
                <li>
                    <a href="index.php?page=exportProductsXML&admin=true">Export Products XML</a>
                </li>
                <li>
                    <a href="index.php?page=exportCategoriesXML&admin=true">Export Categories XML</a>
                </li>
            </ul>
        </section>
    </div>
    {if isset($oNewInfo)}<p><b>{$oNewInfo}</b></p>{/if}
{include file='../layout/footer.tpl'}