{include file='../layout/header.tpl'}
    <div id="main" class="content" role="main">
        <section class="main">
            <ul>
                {foreach $oCategoryInfo as $oCategory}
                    <li>
                        <a href="index.php?page=editCategory&admin=true&id={$oCategory->id}"><img src="img/menu/{$oCategory->picture}.png">{$oCategory->name}</a>
                        <a href="index.php?page=deleteCategory&admin=true&id={$oCategory->id}"><b>x</b></a>
                    </li>
                {/foreach}
            </ul>
        </section>
    </div>
    {if isset($oNewInfo)}<p><b>{$oNewInfo}</b></p>{/if}
    <div id="mainFooter">
        <section class="content menu menuEdit">
            <h2>Produkte</h2>
            <ul class="showMenu">
                <li class="nameHeader">Name</li>
                <li class="nameHeader">Preis</li>
            </ul>
            {foreach from=$oProductInfo item=oInfo key=key name=loop}
                <ul class="showMenu">
                    <li><span class="name">{$oInfo->name}</span>, {$oInfo->description}</li>
                    <li>{$oInfo->cost}</li>
                </ul>
            {/foreach}
        </section>
    </div>
{include file='../layout/footer.tpl'}