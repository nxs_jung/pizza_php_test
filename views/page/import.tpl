{include file='../layout/header.tpl'}
    <div id="main" class="content" role="main">
        <section class="main">
            <ul>
                <li>
                    <a href="index.php?page=uploadProductXML&admin=true">Import XML Product</a>
                </li>
                <li>
                    <a href="index.php?page=uploadCategoryXML&admin=true">Import XML Category</a>
                </li>
            </ul>
        </section>
    </div>
    {if isset($oNewInfo)}<p><b>{$oNewInfo}</b></p>{/if}
{include file='../layout/footer.tpl'}