{include file='../layout/header.tpl'}
    <div id="main" class="content" role="main">
        <section class="main">
            <ul>
                {foreach $oCategoryInfo as $oCatInfo}
                    <li{if $sNameSpies == {$oCatInfo->name} } class="active" {/if}>
                        <a href="index.php?page=mainAction&spies={$oCatInfo->name}&cat_ID={$oCatInfo->id}">
                            <img src="img/menu/{$oCatInfo->picture}.png">{$oCatInfo->name}
                        </a>
                    </li>
                {/foreach}
            </ul>
        </section>
    </div>
       <div id="mainFooter">
        <section class="content">
            <ul class="showMenu">

                <li class="nameHeader">{$sNameSpies|@ucwords}</li>
                <li class="nameHeader">Preis</li>
            </ul>
            {foreach from=$oProductInfo item=oInfo key=key name=loop}
                <ul class="showMenu">
                    <li><span class="name">{$oInfo->name}</span>, {$oInfo->description}</li>
                    <li>{$oInfo->cost}</li>
                </ul>
            {/foreach}
        </section>
    </div>
{include file='../layout/footer.tpl'}