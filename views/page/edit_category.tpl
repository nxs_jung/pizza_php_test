{include file='../layout/header.tpl'}
    <div id="main" class="content" role="main">
        <section class="main">
            <ul>
                <li>
                    <a href="index.php?page=editCategory&admin=true&id={$oCategoryInfo->id}"><img src="img/menu/{$oCategoryInfo->picture}.png">{$oCategoryInfo->name}</a>
                    <a href="index.php?page=deleteCategory&admin=true&id={$oCategoryInfo->id}"><b>x</b></a>
                </li>
                <li>
                    <a href="admin.html">Zurück</a>
                </li>
            </ul>
        </section>
        {if isset($oNewInfo)}<p><b>{$oNewInfo}</b></p>{/if}
        {if isset($oCategoryAddInfo)}<p><b>{$oCategoryAddInfo}</b></p>{/if}
    </div>
    <div id="mainFooter">
        <section class="content menu menuEdit">
            <h2>Produkte</h2>
            <ul class="showMenu">
                <li class="nameHeader">Name</li>
                <li class="nameHeader">Preis</li>
                <li class="nameHeader">Zur Kategorie</li>
            </ul>
            {foreach from=$oProductInfo item=oInfo key=key name=loop}
                <ul class="showMenu">
                    <li><span class="name">{$oInfo->name}</span>, {$oInfo->description}</li>
                    <li>{$oInfo->cost}</li>
                    {assign var=p2cset value=$P2C->getOneProduct2Category($oInfo->id, $oCategoryInfo->id)}
                    {if !$p2cset}
                    <li><a href="index.php?page=addProToCat&admin=true&catID={$oCategoryInfo->id}&proID={$oInfo->id}"><b>+</b></a></li>
                    {/if}
                </ul>
            {/foreach}
        </section>
    </div>
{include file='../layout/footer.tpl'}