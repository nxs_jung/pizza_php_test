-- --------------------------------------------------------
-- Host:                         localhost
-- Server Version:               5.5.35-0ubuntu0.12.04.2 - (Ubuntu)
-- Server Betriebssystem:        debian-linux-gnu
-- HeidiSQL Version:             8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Datenbank Struktur für romantica
DROP DATABASE IF EXISTS `romantica`;
CREATE DATABASE IF NOT EXISTS `romantica` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `romantica`;


-- Exportiere Struktur von Tabelle romantica.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `cost` float(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle romantica.product: ~14 rows (ungefähr)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `name`, `description`, `cost`) VALUES
  (1, 'Pizza Margerita', 'mit Tomanten und Käse', 3.00),
  (2, 'Pizza Napoli', 'Tomaten, Käse, Oliven', 4.00),
  (3, 'Pizza Salami', 'Tomanten, Käse, Salami', 4.00),
  (4, 'Pizza Spinaci', 'Tomaten, Käse, Spinat, Knoblauch', 4.00),
  (5, 'Antipasto Casareccia', 'Gemüsevorspeise', 6.00),
  (6, 'Titamisu', 'Mascarpone, Löffelbiskuit, Espresso und Amaretto', 4.00),
  (7, 'Gamberoni alla Griglia', 'Garnelen vom Grill mit Kräutersauce', 15.00),
  (8, 'Bistecca alla Griglia', 'Rumpsteak mit Beilage', 11.50),
  (9, 'Bistecca Casa', 'Rumpsteack mit Champignons, Zwiebeln', 12.50),
  (10, 'Cola', '1,0L', 2.50),
  (11, 'Cola Light', '1,0L', 2.50),
  (12, 'Fanta', '1,0L', 2.50),
  (13, 'Spaghetti Bolognese', 'mit Hackfleisch und Bolognesesauce', 5.00),
  (14, 'Salat Ruloca', 'Rucolasalat mit Parmesan', 6.50);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
